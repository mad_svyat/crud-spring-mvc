<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html lang="en">
<html>
<head>
    <title>About</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"/>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/bootstrap.css"/>'>
</head>
<body>
<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Simplest CRUD webapp</a>
        <ul class="nav">
            <li>
                <a href="/users/listUsers">Home</a>
            </li>
            <li class="active">
            <a href="index.jsp">About</a>
        </li>
        </ul>
    </div>
</div>
<div class="container">
    <h2>Тестовое задание</h2>
    <h5>Небольшие соображения:</h5>
    <ul >
        <li >Не уверен, нужно ли было после добавления пользователя делать переход на последнюю страницу,
            чтобы убедиться, что он добавлен нормально. Вообщем, этот переход делать не стал.</li>
        <li>При сохранении/удалении пользователя на странице с результатами поиска осущеставляется переход на предыдущую страницу.
        Хорошо это или плохо, тоже не могу сказать.</li>
        <li>Толком не въехал в Hibernate Search, поэтому поиск получился самый простой: по полю 'name'</li>
        <li>Использовал Bootstrap версии 2.3.2, а не последней. Чисто случайно наткнулся на версию 2.3.2 и ее документацию,
            потом переходить на последнюю стало лень, итак потратил большую часть времени на UI</li>
    </ul>
    <br>
    CRUD непосредственно <a href="/users/listUsers">находится здесь</a>
</div>
</body>
</html>

function validateInputs() {
	$("#userForm").validate({
		rules : {
			name : {
				minlength : 1,
				maxlength : 25,
				required : true
			},
			age : {
				max : 120,
				min : 0,
				required :true,
				digits : true
			}
		},

		highlight: function(element) {
			$(element).closest('.control-group').removeClass('success').addClass('error');
		},

		success: function(element) {
			$(element)
				.text('OK!').addClass('valid')
				.closest('.control-group').removeClass('error').addClass('success');
		}
	});
}

function addUser(page, usersOnPage) {
	$.get("emptyUserForm/?page=" + page + "&usersOnPage=" + usersOnPage, function(result) {
		$("#userDialog").html(result).modal('show');
		validateInputs();
	});
}

function editUser(id, page, usersOnPage) {
	$.get("get/" + id + "/?page=" + page + "&usersOnPage=" + usersOnPage, function(result) {
		$("#userDialog").html(result).modal('show');
		validateInputs();
	});
}

function submitSaveForm() {
	$("#userForm").submit();
}



<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"/>
    <title>List Of Users</title>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/bootstrap.css"/>'>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/validation.css"/>'>
</head>
<body>

<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Simplest CRUD webapp</a>
        <ul class="nav">
            <li class="active"><a href="/users/listUsers">Home</a></li>
            <li><a href="/">About</a></li>
        </ul>
    </div>
</div>


<div class="container">


    <div class="row">
        <div class="span4">
            <button class="btn" onclick="addUser(${pageIndex}, ${tableProperties.usersOnPage})"><i class="icon-plus"></i> Add User</button>
        </div>
        <div class="span8" align="right">
            <form:form cssClass="form-search" commandName="searchQuery" method="post" role="form"
                       action="/users/search?page=${pageIndex}&usersOnPage=${tableProperties.usersOnPage}">
                <form:input path="query" id="query" type="text" cssClass="input-medium search-query"></form:input>
                <button type="submit" class="btn">Search</button>
            </form:form>
        </div>
    </div>


    <div class="hide" id="userDialog">
    </div>

 <%--   <c:url var="actionUrl" value="/users/save/${pageIndex}&usersOnPage=${tableProperties.usersOnPage}"/> --%>

    <%-- TABLE --%>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover ">
            <%--   <caption>Users list</caption> --%>
            <thead>
            <tr>
                <th>#</th>
                <th>Id</th>
                <th width="30%">Name</th>
                <th>Age</th>
                <th width="7%">Admin</th>
                <th>Created date</th>
                <th width="8%">Options</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${usersList}" var="user" varStatus="loopCounter">
                <tr>
                    <td><c:out value="${loopCounter.count + (pageIndex - 1) * tableProperties.usersOnPage}"/></td>
                    <td><c:out value="${user.id}"/></td>
                    <td><c:out value="${user.name}"/></td>
                    <td><c:out value="${user.age}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${user.admin}">
                                <img src="/web-resources/img/true.png" alt="true">
                            </c:when>
                            <c:otherwise>
                                <img src="/web-resources/img/false.png" alt="false">
                            </c:otherwise>
                        </c:choose></td>
                    <td>
                        <fmt:formatDate type="both" value="${user.createdDate}"/></td>
                    <td>
                        <nobr>
                            <a class="btn btn-primary btn-small"
                               onclick="editUser(${user.id}, ${pageIndex}, ${tableProperties.usersOnPage})">
                                <i class="icon-white icon-pencil"></i> Edit</a>
                            <a class="btn btn-danger btn-small"
                               href="delete/${user.id}/?page=${pageIndex}&usersOnPage=${tableProperties.usersOnPage}"
                               onclick="return confirm('Are you sure you want to delete this user?');">
                                <i class="icon-white icon-remove"></i> Delete</a>
                        </nobr>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <c:url var="firstUrl" value="/users/?page=1&usersOnPage=${tableProperties.usersOnPage}"/>
    <c:url var="lastUrl" value="/users/?page=${pagesCount}&usersOnPage=${tableProperties.usersOnPage}"/>
    <c:url var="prevUrl" value="/users/?page=${pageIndex - 1}&usersOnPage=${tableProperties.usersOnPage}"/>
    <c:url var="nextUrl" value="/users/?page=${pageIndex + 1}&usersOnPage=${tableProperties.usersOnPage}"/>

    <div class="row">
        <div class="span4">
            <label class="controls-row" for="usersOnPage">Users on page</label>
            <form:form commandName="tableProperties" method="post" role="form" action="/users/listUsers">
                <form:select path="usersOnPage" id="usersOnPage" cssClass="input-mini" onchange="this.form.submit()">
                    <form:option value="15">15</form:option>
                    <form:option value="20">20</form:option>
                    <form:option value="30">30</form:option>
                    <form:option value="50">50</form:option>
                </form:select>
            </form:form>
        </div>
        <div class="span8" align="right">
            <div class="pagination" align="right">
                <ul>
                    <c:choose>
                        <c:when test="${pageIndex == 1}">
                            <li class="disabled"><a href="#">&lt;&lt;</a></li>
                            <li class="disabled"><a href="#">&lt;</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="${firstUrl}">&lt;&lt;</a></li>
                            <li><a href="${prevUrl}">&lt;</a></li>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach var="i" begin="1" end="${pagesCount}">
                        <c:url var="pageUrl" value="/users/?page=${i}&usersOnPage=${tableProperties.usersOnPage}"/>
                        <c:choose>
                            <c:when test="${i == pageIndex}">
                                <li class="active"><a href="${pageUrl}"><c:out value="${i}"/></a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="${pageUrl}"><c:out value="${i}"/></a></li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${pageIndex == pagesCount}">
                            <li class="disabled"><a href="#">&gt;</a></li>
                            <li class="disabled"><a href="#">&gt;&gt;</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="${nextUrl}">&gt;</a></li>
                            <li><a href="${lastUrl}">&gt;&gt;</a></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>

    <script type="text/javascript"
            src='<c:url value="/web-resources/js/lib/jquery-2.1.3.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/web-resources/js/lib/jquery.validate.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/web-resources/js/lib/bootstrap.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/web-resources/js/js-for-listUsers.js"/>'></script>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"/>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/bootstrap.css"/>'>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/validation.css"/>'>
</head>
<body>

<c:url var="actionUrl" value="/users/save/${pageIndex}/?usersOnPage=${tableProperties.usersOnPage}"/>

<div id="userDialog" class="modal" role="dialog">
    <div class="modal-header">
        <h3>Save user</h3>
    </div>
    <div class="modal-body container">
        <div class="row-fluid">
            <div class="span6">
                <div class="area">
                    <form:form cssClass="form-horizontal" id="userForm" commandName="user" method="post" role="form"
                               action="${actionUrl}">
                        <div class="control-group">
                            <label class="control-label" for=
                                    "name">Username</label>
                            <div class="controls">
                                <form:input path="name" id="name" placeholder=
                                        "User name" type="text"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="age">Age</label>
                            <div class="controls">
                                <form:input name="age" id="age" path="age" placeholder=
                                        "0" type="text"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <label class="checkbox">
                                    <form:checkbox path="admin" name="admin"/>
                                    Admin</label>
                            </div>
                        </div>
                        <form:input path="id" type="hidden"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Dismiss</button>
        <button type="submit" class="btn btn-success" onclick="submitSaveForm()">Save User</button>
    </div>
</div>

<!--  It is advised to put the <script> tags at the end of the document body so they don't block rendering of the page -->
<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/jquery-2.1.3.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/jquery.validate.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/bootstrap.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/js-for-listUsers.js"/>'></script>
</body>
</html>


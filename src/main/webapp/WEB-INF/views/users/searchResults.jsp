<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html lang="en">
<html>
<head>
    <title>Search results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"/>
    <link rel="stylesheet"
          href='<c:url value="/web-resources/css/bootstrap.css"/>'>
</head>
<body>
<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Simplest CRUD webapp</a>
        <ul class="nav">
            <li>
                <a href="/users/listUsers?page=${pageIndex}&usersOnPage=${tableProperties.usersOnPage}">Home</a>
            </li>
            <li class="active">
                <a href="#">Search results</a>
            </li>
            <li>
                <a href="/">About</a>
            </li>
        </ul>
    </div>
</div>

<c:set var="count" value="${usersList.size()}"/>

<div class="hide" id="userDialog">
</div>

<div class="container">
    <h4>Results for <i>${searchQuery.query}</i></h4>
    <c:choose>
        <c:when test="${count == 0}">
            No users with name <i>${searchQuery.query}</i> found
        </c:when>
        <c:otherwise>
            Found ${count} users
            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover ">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Id</th>
                        <th width="30%">Name</th>
                        <th>Age</th>
                        <th width="7%">Admin</th>
                        <th>Created date</th>
                        <th width="8%">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${usersList}" var="user" varStatus="loopCounter">
                        <tr>
                            <td><c:out value="${loopCounter.count}"/></td>
                            <td><c:out value="${user.id}"/></td>
                            <td><c:out value="${user.name}"/></td>
                            <td><c:out value="${user.age}"/></td>
                            <td>
                                <c:choose>
                                    <c:when test="${user.admin}">
                                        <img src="/web-resources/img/true.png" alt="true">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="/web-resources/img/false.png" alt="false">
                                    </c:otherwise>
                                </c:choose></td>
                            <td>
                                <fmt:formatDate type="both" value="${user.createdDate}"/></td>
                            <td>
                                <nobr>
                                    <a class="btn btn-primary btn-small"
                                       onclick="editUser(${user.id}, ${pageIndex}, ${tableProperties.usersOnPage})">
                                        <i class="icon-white icon-pencil"></i> Edit</a>
                                    <a class="btn btn-danger btn-small"
                                       href="delete/${user.id}/?page=${pageIndex}&usersOnPage=${tableProperties.usersOnPage}"
                                       onclick="return confirm('Are you sure you want to delete this user?');">
                                        <i class="icon-white icon-remove"></i> Delete</a>
                                </nobr>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:otherwise>
    </c:choose>
    <br>
    <a href="/users/listUsers?page=${pageIndex}&usersOnPage=${tableProperties.usersOnPage}">Back to users list</a>
</div>

<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/jquery-2.1.3.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/jquery.validate.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/lib/bootstrap.js"/>'></script>
<script type="text/javascript"
        src='<c:url value="/web-resources/js/js-for-listUsers.js"/>'></script>
</body>
</html>

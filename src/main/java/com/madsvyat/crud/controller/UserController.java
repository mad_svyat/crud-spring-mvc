package com.madsvyat.crud.controller;

import com.madsvyat.crud.model.SearchQuery;
import com.madsvyat.crud.model.TableProperties;
import com.madsvyat.crud.model.User;
import com.madsvyat.crud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping({"/", "/listUsers"})
    public String listUsers(@RequestParam(required = false, value = "page", defaultValue = "1") int page,
                            @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage,
                            ModelMap map) {
        TableProperties tableProperties = new TableProperties();
        tableProperties.setUsersOnPage(usersOnPage);
        map.addAttribute("user", new User());
        map.addAttribute("searchQuery", new SearchQuery());
        map.addAttribute("pageIndex", page);
        map.addAttribute("tableProperties", tableProperties);
        map.addAttribute("pagesCount", userService.getPagesCount(tableProperties.getUsersOnPage()));
        map.addAttribute("usersList", userService.getUsersList(page, tableProperties.getUsersOnPage()));

        return "/users/listUsers";
    }

    @RequestMapping("/get/{userId}/")
    public String getUser(@PathVariable long userId,
                          @RequestParam(required = false, value = "page", defaultValue = "1") int page,
                          @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage,
                          ModelMap map) {
        User user = userService.getUser(userId);
        map.addAttribute("user", user);
        map.addAttribute("pageIndex", page);

        TableProperties tableProperties = new TableProperties();
        tableProperties.setUsersOnPage(usersOnPage);
        map.addAttribute("tableProperties", tableProperties);

        return "/users/userForm";
    }

    @RequestMapping("/emptyUserForm")
    public String getEmptyForm(@RequestParam(required = false, value = "page", defaultValue = "1") int page,
                               @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage,
                               ModelMap map) {
        map.addAttribute("user", new User());
        map.addAttribute("pageIndex", page);
        TableProperties tableProperties = new TableProperties();
        tableProperties.setUsersOnPage(usersOnPage);
        map.addAttribute("tableProperties", tableProperties);

        return "users/userForm";
    }

    @RequestMapping(value = "/save/{pageIndex}/", method = RequestMethod.POST)
    public String saveUser(@PathVariable int pageIndex, @ModelAttribute User user,
                          @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage,
                          ModelMap map) {
        userService.saveUser(user);
        TableProperties tableProperties = new TableProperties();
        map.addAttribute("tableProperties", tableProperties);

        return "redirect:/users/?page=" + pageIndex + "&usersOnPage=" + usersOnPage;
    }

    @RequestMapping(value = "/delete/{userId}")
    public String deleteUser(@PathVariable long userId,
                             @RequestParam(required = false, value = "page", defaultValue = "1") int page,
                             @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage) {
        userService.deleteUser(userId);

        return "redirect:/users/listUsers?page=" + page + "&usersOnPage=" + usersOnPage;
    }


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchUser(@ModelAttribute SearchQuery searchQuery,
                             @RequestParam(required = false, value = "page", defaultValue = "1") int page,
                             @RequestParam(required = false, value = "usersOnPage", defaultValue = "15") int usersOnPage,
                             ModelMap map) {
        String query = searchQuery.getQuery().trim();
        if (StringUtils.isEmpty(query)) {
            return "redirect:/users/listUsers?page=" + page + "&usersOnPage=" + usersOnPage;
        }

        map.put("searchQuery", searchQuery);
        List<User> users = userService.searchUsersByName(query);
        map.put("usersList", users);
        map.addAttribute("user", new User());
        map.addAttribute("pageIndex", page);
        TableProperties tableProperties = new TableProperties();
        tableProperties.setUsersOnPage(usersOnPage);
        map.addAttribute("tableProperties", tableProperties);

        return "/users/searchResults";
    }
}

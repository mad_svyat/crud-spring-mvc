package com.madsvyat.crud.model;

import javax.persistence.*;

import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.StandardFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;

import java.util.Date;


@Entity
@Indexed
@Table(name = "User")
@AnalyzerDef(name = "nameanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = StandardFilterFactory.class)
        })
public class User {
    long id;
    String name;
    int age;
    boolean isAdmin;
    Date createdDate;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    @Column(nullable = false, length = 25)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Analyzer(definition = "nameanalyzer")
    public String getName() {
        return name;
    }

    @Column
    public int getAge() {
        return age;
    }

    @Column(name = "isAdmin")
    public boolean isAdmin() {
        return isAdmin;
    }

    @PrePersist
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "createdDate")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

package com.madsvyat.crud.model;


public class TableProperties {

    private int usersOnPage;

    public TableProperties() {
        usersOnPage = 10;
    }

    public int getUsersOnPage() {
        return usersOnPage;
    }

    public void setUsersOnPage(int usersOnPage) {
        this.usersOnPage = usersOnPage;
    }
}

package com.madsvyat.crud.dao.impl;

import com.madsvyat.crud.dao.UserDAO;
import com.madsvyat.crud.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void saveUser(User user) {
        getSession().merge(user);
    }

    @Override
    public User getUser(long id) {
        return (User) getSession().get(User.class, id);
    }

    @Override
    public void deleteUser(long id) {
        User user = getUser(id);
        if (user != null) {
            getSession().delete(user);
        }
    }

    @Override
    public List<User> getUsersList(int page, int usersOnPage) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.setFirstResult(usersOnPage * (page - 1));
        criteria.setMaxResults(usersOnPage);

        return criteria.list();
    }

    @Override
    public long getPagesCount(int usersOnPage) {
        long totalUsers = getUsersCount();
        long pagesCount = (totalUsers % usersOnPage == 0) ? totalUsers / usersOnPage : (totalUsers / usersOnPage) + 1;
        return pagesCount;
    }

    @Override
    public long getUsersCount() {
        Criteria countCriteria = getSession().createCriteria(User.class);
        countCriteria.setProjection(Projections.rowCount());
        return (Long) countCriteria.uniqueResult();
    }

    @Override
    public List<User> searchUsersByName(String nameQuery) {

        FullTextSession fullTextSession = Search.getFullTextSession(getSession());
        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(User.class).get();
        org.apache.lucene.search.Query query = qb
                .keyword().onField("name")
                .matching(nameQuery)
                .createQuery();

        org.hibernate.Query hibQuery =
                fullTextSession.createFullTextQuery(query, User.class);

        List<User> results = hibQuery.list();

        return results;
    }

    private Session getSession() {
        Session session = sessionFactory.getCurrentSession();
        if (session == null) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}

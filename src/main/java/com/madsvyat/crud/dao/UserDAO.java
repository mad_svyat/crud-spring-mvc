package com.madsvyat.crud.dao;

import com.madsvyat.crud.model.User;

import java.util.List;

public interface UserDAO {
    /**
     * Create or Update
     * @param user
     */
    void saveUser(User user);

    /**
     * Read user by id
     * @param id user id
     * @return
     */
    User getUser(long id);

    /**
     * Delete user
     * @param id user id
     */
    void deleteUser(long id);

    /**
     * Read users list
     * @param page page index
     * @param usersOnPage page size
     * @return
     */
    List<User> getUsersList(int page, int usersOnPage);

    long getPagesCount(int usersOnPage);

    long getUsersCount();

    List<User> searchUsersByName(String nameQuery);
}

package com.madsvyat.crud.service.impl;

import com.madsvyat.crud.dao.UserDAO;
import com.madsvyat.crud.model.User;
import com.madsvyat.crud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDAO userDAO;

    @Override
    @Transactional
    public void saveUser(User user) {
        userDAO.saveUser(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User getUser(long id) {
        return userDAO.getUser(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getUsersList(int page, int usersOnPage) {
        return userDAO.getUsersList(page, usersOnPage);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> searchUsersByName(String query) {
        return userDAO.searchUsersByName(query);
    }

    @Override
    @Transactional
    public long getPagesCount(int usersOnPage) {
        return userDAO.getPagesCount(usersOnPage);
    }

    @Override
    @Transactional
    public void deleteUser(long id) {
        userDAO.deleteUser(id);
    }
}

package com.madsvyat.crud.service;

import com.madsvyat.crud.model.User;

import java.util.List;

/**
 * Created by N1 on 24.12.2014.
 */
public interface UserService {
    /**
     * Create or Update
     * @param user
     */
    void saveUser(User user);

    /**
     * Read user by id
     * @param id user id
     * @return
     */
    User getUser(long id);

    /**
     * Read users list
     * @param page page index
     * @param usersOnPage page size
     * @return
     */
    List<User> getUsersList(int page, int usersOnPage);

    List<User> searchUsersByName(String query);

    long getPagesCount(int usersOnPage);

    /**
     * Delete user
     * @param id
     */
    void deleteUser(long id);
}
